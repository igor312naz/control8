<?php

namespace AppBundle\Controller;

use AppBundle\Form\Register;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegisterController extends Controller
{
    /**
     * @Route("/register/ticket", name="ticket")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function RegisterAction(Request $request)
    {
        $form = $this->createForm(Register::class);
        $showForm = $form->createView();


       // $form->handleRequest($request);

        return $this->render('@App/Base/register.html.twig', array(
            'form' => $showForm,
        ));
    }

}
