<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $books = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->findAll();

        $category = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();


        return $this->render('@App/Base/index.html.twig', array(
            'books'=>$books,
            'category'=>$category,
        ));
    }

    /**
     * @Route("/category/{id}",requirements={"id": "\d+"}, name="category")
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(int $id)
    {
        $books = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->getBookByCategory($id);

        return $this->render('@App/Base/category.html.twig',array(
            'books'=>$books
        ));



    }
}
