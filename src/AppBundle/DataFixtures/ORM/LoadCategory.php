<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategory extends Fixture
{
    public const CATEGORY_ONE = 'Роман';
    public const CATEGORY_TWO = 'Драма';

    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1
            ->setName('Роман');


        $manager->persist($category1);

        $category2 = new Category();
        $category2
            ->setName('Драма');


        $manager->persist($category2);
        $manager->flush();

        $this->addReference(self::CATEGORY_ONE, $category1);
        $this->addReference(self::CATEGORY_TWO, $category2);
    }
}
