<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBooks extends Fixture
{


    public function load(ObjectManager $manager)
    {
        $namesOfBooks = [
            "Мастер и маргарита",
            "На дне",
            "Война и мир"
        ];

        $namesOfName = [
            "М. Булгаков",
            "М. Горький",
            "Л. Толстой"
        ];


        for ($i = 0; $i <= 2; $i++) {
            $book = new Book();
            $book
                ->setName($namesOfBooks[$i])
                ->setAuthor($namesOfName[$i])
                ->setImage("pic{$i}".".jpeg")
                ->setStatus(1);
                //->setCategory("1");   тут нужно руками в таблице связь добоавить id и тогда првязка заработает :)
            $manager->persist($book);
        }


        $manager->flush();

    }

    function getDependencies()
    {
        return array(
            LoadCategory::class
        );
    }
}
