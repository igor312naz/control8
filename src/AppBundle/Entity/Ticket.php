<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="string", length=255)
     */
    private $passport;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_id", type="integer")
     */
    private $ticket_id;

    /**
     * @var Book[]
     *
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="ticket")
     */
    private $books;

    /**
     * @var Order_for_book[]
     *
     * @ORM\OneToMany(targetEntity="Order_for_book", mappedBy="ticket")
     */
    private $orders;


    public function __construct()
    {
        $this->books = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ticket
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $address
     *
     * @return Ticket
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $passport
     * @return Ticket
     */
    public function setPassport(string $passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param string $ticket_id
     * @return Ticket
     */
    public function setTicketId(string $ticket_id)
    {
        $this->ticket_id = $ticket_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTicketId()
    {
        return $this->ticket_id;
    }

    /**
     * @return Book[]
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * @param Book $book
     * @return Ticket
     */
    public function addBook(Book $book)
    {
        $this->books->add($book);
        return $this;
    }


    /**
     * @param Order_for_book $orders
     * @return $this
     */
    public function addOrders(Order_for_book $orders)
    {
        $this->orders->add($orders);
        return $this;
    }


    /**
     * @return Order_for_book[]
     */
    public function getOrders(): array
    {
        return $this->orders;
    }
}

